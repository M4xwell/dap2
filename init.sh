#!/bin/sh

mvn archetype:generate -DgroupId=com.max.$1 -DartifactId=$1 -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false

cp Makefile $1
cp .gitlab-ci.yml $1
cp .gitignore $1
cp ./gl.cfg $1
cp ./gl-project.py $1

python3 gl-project.py $1

cd $1

git init
git add .
git commit -m "init commit" 
git remote add origin git@gitlab.com:dap2/$1.git
git push -u origin master

rm gl.cfg
rm gl-project.py