default: build-run

#build the app
build:
	@mvn compile -DskipTests

#run JUnit tests
test:
	@mvn test

#run the app
run:
	@java -cp target/classes/ com.max.blatt06.App

#build and run the app without testing
build-run: build run

#build, test and run the app
all: build test run
